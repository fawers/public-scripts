#!/usr/bin/env python

from re import search
from subprocess import Popen, PIPE, call as sys_call


OUTPUT_DEVICE = 'LVDS1'


process = Popen('gtf 1366 768 60', shell=True, stdout=PIPE)
process.wait()

output = process.stdout.read().decode().strip()

match = search(r'Modeline "(.*)"\s*(.*)$', output)

if match:
    name = match.group(1).replace('1368', '1366')
    command = match.group(2).replace('1368', '1366')

    # create new mode
    sys_call('xrandr --newmode "{}" {}'.format(name, command), shell=True)

    # add new mode to OUTPUT_DEVICE
    sys_call('xrandr --addmode {} {}'.format(OUTPUT_DEVICE, name), shell=True)

    # activate new mode on OUTPUT_DEVICE
    sys_call('xrandr --output {} --mode {}'.format(OUTPUT_DEVICE, name),
             shell=True)

else:
    print('Regex didn\'t match!')

