#!/usr/bin/env python2
import os
from signal import signal, SIGINT, SIGTERM, pause
from datetime import datetime, timedelta

# ===========================================

HOME_DIR = os.environ['HOME']
FILE_NAME = ".pytimer-lock"
FULL_NAME = os.path.join(HOME_DIR, FILE_NAME)

def get_time(stack):
    start_time = stack.f_locals['now']
    end_time = datetime.now()
    offset = end_time - start_time
    time = str(timedelta(seconds=offset.seconds))

    return time

def notify(message):
    message = "'%s'" % message
    os.system("notify-send -i appointment -u critical 'PyTimer' " + message)

def handle_int(signum, stack):
    message = get_time(stack)
    notify(message)

def handle_term(signum, stack):
    message = get_time(stack)
    message += "\nTerminating..."
    notify(message)
    os.remove(FULL_NAME)
    exit(signum)


if __name__ == '__main__':
    if os.path.exists(FULL_NAME):

        with open(FULL_NAME) as f:
            pid = int(f.readline())

        if '-t' in os.sys.argv:
            os.kill(pid, SIGTERM)

        else:
            os.kill(pid, SIGINT)

    else:
        pid = os.getpid()

        with open(FULL_NAME, 'w') as f:
            f.write("%i\n" % pid)

        now = datetime.now()
        signal(SIGINT, handle_int)
        signal(SIGTERM, handle_term)

        while 1:
            pause()

