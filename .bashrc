# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## Enable bash completion
if [ -f /etc/bash_completion ]; then
      . /etc/bash_completion
fi


## Add ssh identities if none is found while connected to `sgi` or `werneck`
if [[ "$(nmcli -n d | grep -Eio '(sgi|werneck)')" != "" &&
      "$(ssh-add -l | grep 'no identities')" != "" ]]; then
    ssh-add
fi


xhost +local:root > /dev/null 2>&1

complete -cf sudo

shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete

export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoreboth
export JAVA_FONTS=/usr/share/fonts/TTF
export EDITOR=/usr/bin/nano
export PATH=$PATH:/home/fawers/.gem/ruby/2.2.0/bin

alias pacman='pacman --color auto'
alias spacman='sudo pacman --color=auto'
alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -la --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip -E'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano PKGBUILD'
alias vsftpd='sudo vsftpd'
alias qshell='sleep 3; exit'
alias fixit='sudo rm -f /var/lib/pacman/db.lck && sudo pacman-mirrors -g && sudo pacman -Syyuu  && sudo pacman -Suu'
alias fixflux='killall fluxgui; rm /tmp/fluxgui_fawers.pid ; fluxgui &'
alias fixmega='killall megasync; rm "/home/fawers/.local/share/data/Mega Limited/MEGAsync/megasync.lock" ; megasync &'
alias lsm="ls | grep -E '\.(mkv|mp4)$'"
alias lenv='. ~/.lenv'
alias lenvh='. ~/.lenvh'
alias battery="upower -i $(upower -e | grep BAT) | grep '(state|time to full|percentage):'"
alias ssh_init='. ~/.ssh_init.sh'

## Utils
function parse_git_branch {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}
source ~/.autoenv/activate.sh
source ~/.git-completion.bash

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# prompt
PS1="\[\e[1;35m\]\u\[\e[37m\]@\[\e[32m\]\h \[\e[34m\]\w \[\e[\$(gitstatus)m\]\$(parse_git_branch)\n\[\e[1;31m\]\$\[\e[0m\] "
