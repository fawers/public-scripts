#!/bin/sh

if [[ $(ssh-add -l 2>&1) =~ 'not open' ]]; then
    eval "$(ssh-agent)"
fi

if [[ $(ssh-add -l 2>&1) =~ 'no identities' ]]; then
    ssh-add
fi
