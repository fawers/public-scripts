#!/bin/sh
clear
port=$1
if [[ $port == '' ]]; then
  port=8000
fi
python manage.py runserver 0.0.0.0:$port || notify-send -i error 'Server' 'Something wicked just happened here, boss'
