#!/usr/bin/python
# -*- encoding: utf-8 -*-


import os, re
from sys        import argv as args, stderr
from subprocess import Popen, PIPE

def colored_text(text, color_code, bold=False, style=''):
    return '\033[40;3{ccode}{b}{s}m{t}\033[0m'.format(
        ccode=color_code, b=';1' if bold else '', t=text, s=style)

if __name__ == '__main__':
    if len(args) < 2:
        print >>stderr, "Execute o script com um ou mais diretórios."
        print >>stderr, "python %s dir1 [dir2 [dir3 [dir4 [...]]]]" %args[0]
        exit(1)

    home_dir = os.getcwd()
    hosts    = ("Anbient",)

    for entry in args[1:]:
        if not os.path.isdir(entry):
            print >>stderr, "%s não é um diretório. Continuando..." %entry
        else:
            os.chdir(entry)
            for host in hosts:
                name = Popen("ls *01*%s*" %host, shell=1, stdout=PIPE, stderr=PIPE
                    ).stdout.readline().strip()
                if name != '':
                    break
            else:
                print >>stderr, "{}: Host não encontrado em {}!".format(entry, hosts)
                continue

            if host == "Anbient":
                ext     = name[name.rfind('.'):]
                name    = name[:name.find('01')]
                newname = ' '.join(name.split('_')) + '(%%s)%s' %ext
            else:
                newname = ''

            if newname:
                files = os.listdir('.')

                for file in files:
                    try:
                        epnum = re.findall(r'\d{2,}', file)[0]
                        print 'Renaming:\n    {}\nto: {}'.format(colored_text(file, 0b001, 1, ';9'),
                            colored_text(newname %epnum, 0b010, 1, ';4'))
                        os.rename(file, newname %(epnum,))
                    except IndexError:
                        pass

            os.chdir(home_dir)
